﻿using System;
using System.Windows.Forms;

namespace beautyproduct_app
{
    public partial class BeautyProdForm : Form
    {
        private enum Columns
        {
            COLUMN_ENTRYID = 0,
            COLUMN_NAME,
            COLUMN_DESCRIPTION,
            COLUMN_CATEGORY,
            COLUMN_COMPANY
        }

        public BeautyProdForm()
        {
            InitializeComponent();
        }

        private void BeautyProdForm_Load(object sender, EventArgs e)
        {
            this.category_table_adapter.Fill(this.beauty_product_db_dataset.category);
            //category_combo_box.DataSource = beauty_product_db_dataset.Tables["category"];
            //category_combo_box.ValueMember = "EntryID";
            //category_combo_box.SelectedValue = "Name";
            this.beauty_product_table_adapter.FillByCategory(this.beauty_product_db_dataset.beautyproduct, int.Parse(category_combo_box.SelectedValue.ToString()));
            this.category_combo_box.SelectedValueChanged += new System.EventHandler(this.OnComboBoxValueChanged);
        }

        private void OnComboBoxValueChanged(object sender, EventArgs e)
        {
            try
            {
                beauty_product_table_adapter.FillByCategory(beauty_product_db_dataset.beautyproduct, int.Parse(category_combo_box.SelectedValue.ToString()));
            }
            catch(Exception)
            {
                // this is to avoid a crash when closing the app.
            }
        }

        private void OnCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            DataGridViewRow selected_row = beauty_products_grid.Rows[e.RowIndex];
            object name = selected_row.Cells[(int)Columns.COLUMN_NAME].Value,
                description = selected_row.Cells[(int)Columns.COLUMN_DESCRIPTION].Value,
                company = selected_row.Cells[(int)Columns.COLUMN_COMPANY].Value,
                entry_id = selected_row.Cells[(int)Columns.COLUMN_ENTRYID].Value;

            if (name == null || entry_id == null || company == null || description == null)
                return;

            beauty_product_table_adapter.UpdateByEntryID(
                name as string,
                description as string,
                (int)company,
                (int)entry_id
                );

            beauty_product_table_adapter.FillByCategory(beauty_product_db_dataset.beautyproduct, int.Parse(category_combo_box.SelectedValue.ToString()));
        }

        private void OnRowsRemoved(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Cancel)
                return;

            DataGridViewRow deleted_row = e.Row;
            beauty_product_table_adapter.Delete(
                (int)deleted_row.Cells[(int)Columns.COLUMN_ENTRYID].Value,
                deleted_row.Cells[(int)Columns.COLUMN_NAME].Value as string,
                deleted_row.Cells[(int)Columns.COLUMN_DESCRIPTION].Value as string,
                (int)deleted_row.Cells[(int)Columns.COLUMN_COMPANY].Value,
                (int)deleted_row.Cells[(int)Columns.COLUMN_CATEGORY].Value
                );

            beauty_product_table_adapter.FillByCategory(beauty_product_db_dataset.beautyproduct, int.Parse(category_combo_box.SelectedValue.ToString()));
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            addbeautyproductform tmp = new addbeautyproductform();
            tmp.ShowDialog();
            beauty_product_table_adapter.Insert(tmp.name, tmp.description, tmp.company, int.Parse(category_combo_box.SelectedValue.ToString()));
        }
    }
}
