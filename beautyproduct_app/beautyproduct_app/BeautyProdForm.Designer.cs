﻿namespace beautyproduct_app
{
    partial class BeautyProdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.main_panel = new System.Windows.Forms.TableLayoutPanel();
            this.io_panel = new System.Windows.Forms.TableLayoutPanel();
            this.category_combo_box = new System.Windows.Forms.ComboBox();
            this.category_binding_source = new System.Windows.Forms.BindingSource(this.components);
            this.beauty_product_db_dataset = new beautyproduct_app.BeautyProductDBDataSet();
            this.beauty_products_grid = new System.Windows.Forms.DataGridView();
            this.entryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.beautyProductDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.beauty_product_binding_source = new System.Windows.Forms.BindingSource(this.components);
            this.add_btn = new System.Windows.Forms.Button();
            this.beauty_product_table_adapter = new beautyproduct_app.BeautyProductDBDataSetTableAdapters.beautyproductTableAdapter();
            this.category_table_adapter = new beautyproduct_app.BeautyProductDBDataSetTableAdapters.categoryTableAdapter();
            this.main_panel.SuspendLayout();
            this.io_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.category_binding_source)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_product_db_dataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_products_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_product_binding_source)).BeginInit();
            this.SuspendLayout();
            // 
            // main_panel
            // 
            this.main_panel.ColumnCount = 1;
            this.main_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_panel.Controls.Add(this.io_panel, 0, 0);
            this.main_panel.Controls.Add(this.add_btn, 0, 1);
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.Location = new System.Drawing.Point(0, 0);
            this.main_panel.Name = "main_panel";
            this.main_panel.RowCount = 2;
            this.main_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.49097F));
            this.main_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.50903F));
            this.main_panel.Size = new System.Drawing.Size(913, 609);
            this.main_panel.TabIndex = 0;
            // 
            // io_panel
            // 
            this.io_panel.ColumnCount = 1;
            this.io_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.io_panel.Controls.Add(this.category_combo_box, 0, 0);
            this.io_panel.Controls.Add(this.beauty_products_grid, 0, 1);
            this.io_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.io_panel.Location = new System.Drawing.Point(3, 3);
            this.io_panel.Name = "io_panel";
            this.io_panel.RowCount = 2;
            this.io_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.832714F));
            this.io_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95.16729F));
            this.io_panel.Size = new System.Drawing.Size(907, 538);
            this.io_panel.TabIndex = 0;
            // 
            // category_combo_box
            // 
            this.category_combo_box.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.category_combo_box.BackColor = System.Drawing.Color.Gainsboro;
            this.category_combo_box.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.category_binding_source, "Name", true));
            this.category_combo_box.DataSource = this.category_binding_source;
            this.category_combo_box.DisplayMember = "Name";
            this.category_combo_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.category_combo_box.FormattingEnabled = true;
            this.category_combo_box.Location = new System.Drawing.Point(3, 3);
            this.category_combo_box.Name = "category_combo_box";
            this.category_combo_box.Size = new System.Drawing.Size(901, 21);
            this.category_combo_box.TabIndex = 0;
            this.category_combo_box.ValueMember = "EntryID";
            // 
            // category_binding_source
            // 
            this.category_binding_source.DataMember = "category";
            this.category_binding_source.DataSource = this.beauty_product_db_dataset;
            // 
            // beauty_product_db_dataset
            // 
            this.beauty_product_db_dataset.DataSetName = "BeautyProductDBDataSet";
            this.beauty_product_db_dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // beauty_products_grid
            // 
            this.beauty_products_grid.AllowUserToAddRows = false;
            this.beauty_products_grid.AutoGenerateColumns = false;
            this.beauty_products_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.beauty_products_grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.beauty_products_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.beauty_products_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.beauty_products_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entryIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.beautyProductDescriptionDataGridViewTextBoxColumn,
            this.companyIDDataGridViewTextBoxColumn,
            this.categoryIDDataGridViewTextBoxColumn});
            this.beauty_products_grid.DataSource = this.beauty_product_binding_source;
            this.beauty_products_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.beauty_products_grid.GridColor = System.Drawing.Color.SeaGreen;
            this.beauty_products_grid.Location = new System.Drawing.Point(3, 29);
            this.beauty_products_grid.Name = "beauty_products_grid";
            this.beauty_products_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.beauty_products_grid.Size = new System.Drawing.Size(901, 506);
            this.beauty_products_grid.TabIndex = 1;
            this.beauty_products_grid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellValueChanged);
            this.beauty_products_grid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnRowsRemoved);
            // 
            // entryIDDataGridViewTextBoxColumn
            // 
            this.entryIDDataGridViewTextBoxColumn.DataPropertyName = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.HeaderText = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.Name = "entryIDDataGridViewTextBoxColumn";
            this.entryIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // beautyProductDescriptionDataGridViewTextBoxColumn
            // 
            this.beautyProductDescriptionDataGridViewTextBoxColumn.DataPropertyName = "BeautyProductDescription";
            this.beautyProductDescriptionDataGridViewTextBoxColumn.HeaderText = "BeautyProductDescription";
            this.beautyProductDescriptionDataGridViewTextBoxColumn.Name = "beautyProductDescriptionDataGridViewTextBoxColumn";
            // 
            // companyIDDataGridViewTextBoxColumn
            // 
            this.companyIDDataGridViewTextBoxColumn.DataPropertyName = "CompanyID";
            this.companyIDDataGridViewTextBoxColumn.HeaderText = "CompanyID";
            this.companyIDDataGridViewTextBoxColumn.Name = "companyIDDataGridViewTextBoxColumn";
            // 
            // categoryIDDataGridViewTextBoxColumn
            // 
            this.categoryIDDataGridViewTextBoxColumn.DataPropertyName = "CategoryID";
            this.categoryIDDataGridViewTextBoxColumn.HeaderText = "CategoryID";
            this.categoryIDDataGridViewTextBoxColumn.Name = "categoryIDDataGridViewTextBoxColumn";
            // 
            // beauty_product_binding_source
            // 
            this.beauty_product_binding_source.DataMember = "beautyproduct";
            this.beauty_product_binding_source.DataSource = this.beauty_product_db_dataset;
            // 
            // add_btn
            // 
            this.add_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.add_btn.Location = new System.Drawing.Point(3, 547);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(907, 59);
            this.add_btn.TabIndex = 1;
            this.add_btn.Text = "ADD BEAUTY PRODUCT FOR CATEGORY";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // beauty_product_table_adapter
            // 
            this.beauty_product_table_adapter.ClearBeforeFill = true;
            // 
            // category_table_adapter
            // 
            this.category_table_adapter.ClearBeforeFill = true;
            // 
            // BeautyProdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 609);
            this.Controls.Add(this.main_panel);
            this.Name = "BeautyProdForm";
            this.Text = "Beauty products";
            this.Load += new System.EventHandler(this.BeautyProdForm_Load);
            this.main_panel.ResumeLayout(false);
            this.io_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.category_binding_source)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_product_db_dataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_products_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beauty_product_binding_source)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_panel;
        private System.Windows.Forms.TableLayoutPanel io_panel;
        private System.Windows.Forms.ComboBox category_combo_box;
        private System.Windows.Forms.Button add_btn;
        private BeautyProductDBDataSet beauty_product_db_dataset;
        private System.Windows.Forms.BindingSource beauty_product_binding_source;
        private BeautyProductDBDataSetTableAdapters.beautyproductTableAdapter beauty_product_table_adapter;
        private System.Windows.Forms.BindingSource category_binding_source;
        private BeautyProductDBDataSetTableAdapters.categoryTableAdapter category_table_adapter;
        private System.Windows.Forms.DataGridView beauty_products_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn entryIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn beautyProductDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIDDataGridViewTextBoxColumn;
    }
}

