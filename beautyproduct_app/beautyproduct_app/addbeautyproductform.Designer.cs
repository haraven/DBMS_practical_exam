﻿namespace beautyproduct_app
{
    partial class addbeautyproductform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.name_box = new System.Windows.Forms.TextBox();
            this.description_box = new System.Windows.Forms.TextBox();
            this.company_box = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.name_box, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.description_box, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.company_box, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(913, 649);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // name_box
            // 
            this.name_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_box.Location = new System.Drawing.Point(3, 3);
            this.name_box.Name = "name_box";
            this.name_box.Size = new System.Drawing.Size(907, 20);
            this.name_box.TabIndex = 0;
            // 
            // description_box
            // 
            this.description_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.description_box.Location = new System.Drawing.Point(3, 166);
            this.description_box.Name = "description_box";
            this.description_box.Size = new System.Drawing.Size(907, 20);
            this.description_box.TabIndex = 1;
            // 
            // company_box
            // 
            this.company_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.company_box.Location = new System.Drawing.Point(3, 329);
            this.company_box.Name = "company_box";
            this.company_box.Size = new System.Drawing.Size(907, 20);
            this.company_box.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 571);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(907, 75);
            this.button1.TabIndex = 3;
            this.button1.Text = "add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnAddClicked);
            // 
            // addbeautyproductform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 649);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "addbeautyproductform";
            this.Text = "addbeautyproductform";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox name_box;
        private System.Windows.Forms.TextBox description_box;
        private System.Windows.Forms.TextBox company_box;
        private System.Windows.Forms.Button button1;
    }
}