﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace beautyproduct_app
{
    public partial class addbeautyproductform : Form
    {
        public addbeautyproductform()
        {
            InitializeComponent();
        }

        private void OnAddClicked(object sender, EventArgs e)
        {
            name = name_box.Text;
            description = description_box.Text;
            company = int.Parse(company_box.Text);

            Close();
        }

        public string name;
        public string description;
        public int company;
        public int category;
    }
}
